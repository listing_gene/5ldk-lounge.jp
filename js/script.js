$(function () {
    // リンクをクリックしたときの処理。外部リンクやページ内移動のスクロールリンクなどではフェードアウトさせたくないので少し条件を加えてる。
    $('a[href ^= "https://5ldk-lounge.jp"]' + 'a[target != "_blank"]').click(function () {
        var url = $(this).attr('href'); // クリックされたリンクのURLを取得
        setTimeout(function () { location.href = url; }, 800); // URLにリンクする
        return false;
    });
});
// ページのロードが終わった後の処理
$(window).on('load', function () {
    $('#js-loader').delay(300).fadeOut(400); //ローディング画面をフェードアウトさせることでメインコンテンツを表示
    $('body').removeClass('hidden');
});
// ページのロードが終わらなくても10秒たったら強制的に処理を実行
$(function () { setTimeout('stopload()', 10000); });
function stopload() {
    $('#js-loader').delay(300).fadeOut(400); //ローディング画面をフェードアウトさせることでメインコンテンツを表示
    $('body').removeClass('hidden');
};

// ヘッダーアニメーション
window.addEventListener('DOMContentLoaded', function () {
    $(function () {
        var $win = $(window),
            $header = $('header'),
            animationClass = 'is-animation';

        $win.on('load scroll', function () {
            var value = $(this).scrollTop();
            if (value > 200) {
                $header.addClass(animationClass);
            } else {
                $header.removeClass(animationClass);
            }
        });
    });
});

// スムーススクロール
$('a[href^="#"]').click(function () {
    var speed = 500;
    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top - 100;
    $("html, body").animate({
        scrollTop: position
    }, speed, "swing");
    return false;
});

$(window).on('load', function () {
    $('.slider').slick({
        asNavFor: '.slider-nav',
        fade: true,
        arrows: true,
        slidesToShow: 1,
        prevArrow: '<div class="prev"></div>',
        nextArrow: '<div class="next"></div>',
    });
    $('.slider-nav').slick({
        asNavFor: '.slider',
        focusOnSelect: true,
        arrows: false,
        dots: false,
        centerMode: false,
        slidesToShow: 6,
        responsive: [
            {
                breakpoint: 735,
                settings: {
                    slidesToShow: 3,
                    dots: true,
                    centerMode: true,
                }
            }
        ]
    });
});
$(function () {
    $('.slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        if ($('.slider-nav img').length < 7) {
            $('.slider-nav').slick('slickSetOption', 'centerMode', true, true);
        }
    });
});

/* //送信ボタンを無効化する
$('.submit').prop("disabled", true);
//入力欄の操作時
$('form input:required, textarea:required').change(function () {
    //必須項目が空かどうかフラグ
    let flag = true;
    //必須項目をひとつずつチェック
    $('form input:required, textarea:required').each(function (e) {
        //もし必須項目が空なら
        if ($('form input:required, textarea:required').eq(e).val() === "") {
            flag = false;
        }
        //全て埋まっていたら
        if (flag) {
            //送信ボタンを復活
            $('.submit').prop("disabled", false);
        }
        else {
            //送信ボタンを閉じる
            $('.submit').prop("disabled", true);
        }
    });
}); */