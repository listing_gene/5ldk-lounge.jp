<?php
// フォームのボタンが押されたら
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // フォームから送信されたデータを各変数に格納
    $name = $_POST["name"];
    $mail = $_POST["mail"];
    $content  = $_POST["content"];
}

// 送信ボタンが押されたら
if (isset($_POST["submit"])) {

    // 下から受付メール
    // 送信ボタンが押された時に動作する処理をここに記述する
    mb_language("ja");
    mb_internal_encoding("UTF-8");
    // 件名を変数subjectに格納
    $subject = "【お問い合わせ】5LDK";
    // メール本文を変数bodyに格納
    $body = <<< EOM

ホームページ(https://5ldk-lounge.jp)からお問い合わせがありました。

===============================

【 お名前 】
{$name}

【 メールアドレス 】
{$mail}

【 内容 】
{$content}

===============================

EOM;

    // 送信元のメールアドレスを変数fromEmailに格納
    $fromEmail = "$mail";
    $Email = "info1@5ldk-lounge.jp";

    // 送信元の名前を変数fromNameに格納
    $fromName = "5LDK";

    // ヘッダ情報を変数headerに格納する
    $header = "Content-Type:text/html;charset=UTF-8\r\n";
    $header .= "From: info1@5ldk-lounge.jp\r\n";
    $header .= "Return-Path: info1@5ldk-lounge.jp\r\n";
    $param = "-f info1@5ldk-lounge.jp\r\n";
    $header = "From: " . mb_encode_mimeheader($name,"ISO-2022-JP-MS","UTF-8") . "<{$mail}>";
    $name = mb_convert_encoding($name,"ISO-2022-JP-MS","UTF-8");
    $body = mb_convert_encoding($body,"ISO-2022-JP-MS","UTF-8");

    // メール送信を行う
    mb_send_mail($Email, $subject, $body, $header);

    // 送信ボタンが押された時に動作する処理をここに記述する
    mb_language("ja");
    mb_internal_encoding("UTF-8");
    // 件名を変数subjectに格納
    $subject = "【お問い合わせ】5LDK";

    // メール本文を変数bodyに格納
    $body = <<< EOM
{$name}　様

お問い合わせありがとうございます。
以下のお問い合わせ内容を、メールにて確認させていただきました。

===============================

【 お名前 】
{$name}

【 メールアドレス 】
{$mail}

【 内容 】
{$content}

===============================

内容を確認の上、ご連絡させていただきます。
今しばらくお待ちくださいませ。

5LDK (https://5ldk-lounge.jp)
EOM;

    // 送信元のメールアドレスを変数fromEmailに格納
    $fromEmail = "info1@5ldk-lounge.jp";

    // 送信元の名前を変数fromNameに格納
    $fromName = "5LDK";

    // ヘッダ情報を変数headerに格納する
    $header = "Content-Type:text/html;charset=UTF-8\r\n";
    $header .= "From: info1@5ldk-lounge.jp\r\n";
    $header .= "Return-Path: info1@5ldk-lounge.jp\r\n";
    $param = "-f info1@5ldk-lounge.jp";
    $header = "From: " . mb_encode_mimeheader($fromName,"ISO-2022-JP-MS","UTF-8") . "<{$fromEmail}>";
    $name = mb_convert_encoding($name,"ISO-2022-JP-MS","UTF-8");
    $body = mb_convert_encoding($body,"ISO-2022-JP-MS","UTF-8");

    //mb_send_mail("kanda.it.school.trial@gmail.com", "メール送信テスト", "メール本文");
    mb_send_mail($mail, $subject, $body, $header);

    // サンクスページに画面遷移させる
    header("Location: https://5ldk-lounge.jp/thanks.html");
    exit;
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,viewport-fit=cover,user-scalable=no">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="icon" type="image/png" href="img/favicon.svg">
    <title>5LDK | お問い合わせ内容確認</title>
    <meta name="description"
        content="先鋭的なモダンと洗練されたラグジュアリーが融合する、西麻布・完全会員制ラウンジ『5LDK』。真のホスピタリティを追求する確固たる姿勢で、有意義な時間をご提供いたします。特別な空間で特別な出会いを。">
    <meta name=“keywords” content=“5LDK,5ldk,西麻布,完全会員制,ラウンジ,高級“>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5B6TVF6');</script>
    <!-- End Google Tag Manager -->
    <meta property="og:site_name" content="5LDK" />
    <meta property="og:title" content="5LDK | お問い合わせ内容確認" />
    <meta property="og:url" content="https://5ldk-lounge.jp" />
    <meta property="og:type" content="website" />
    <meta name="author" content="5LDK">
    <meta property="og:locale" content="ja_JP">
    <meta property="og:description"
        content="先鋭的なモダンと洗練されたラグジュアリーが融合する、西麻布・完全会員制ラウンジ『5LDK』。真のホスピタリティを追求する確固たる姿勢で、有意義な時間をご提供いたします。特別な空間で特別な出会いを。" />
    <meta property="og:image" content="https://5ldk-lounge.jp/img/og.png" />
    <link rel="preconnect" href="https://5ldk-lounge.jp">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Maven+Pro:wght@700&display=swap" rel="stylesheet">
</head>

<body class="confirm">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5B6TVF6" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <header>
        <div class="inner">
            <a id="logo" class="logo" href="index.html">
                <h1><img src="img/logo.svg" alt="5LDK | 会員制ラウンジ"></h1>
            </a>
            <div id="nav-toggle">
                <div>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>

            <div id="gloval-nav">
                <nav>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="index.html#section-concept">CONCEPT</a></li>
                        <li><a href="index.html#section-system">SYSTEM</a></li>
                        <li><a href="index.html#section-photoGallery">PHOTO GALLERY</a></li>
                        <li><a href="index.html#section-access">ACCESS</a></li>
                        <li><a href="index.html#section-contact">CONTACT</a></li>
                    </ul>
                </nav>
            </div>

            <div id="pc-nav">
                <nav>
                    <ul>
                        <li><a href="index.html#section-concept">CONCEPT</a></li>
                        <li><a href="index.html#section-system">SYSTEM</a></li>
                        <li><a href="index.html#section-photoGallery">PHOTO GALLERY</a></li>
                        <li><a href="index.html#section-access">ACCESS</a></li>
                        <li><a href="index.html#section-contact">CONTACT</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <main>
        <section id="section-confirm" class="section-confirm">
            <div class="head">
                <h3>CONFIRM</h3>
            </div>
            <form action="confirm.php" method="post">
                <input type="hidden" name="name" value="<?php echo $name; ?>">
                <input type="hidden" name="mail" value="<?php echo $mail; ?>">
                <input type="hidden" name="content" value="<?php echo $content; ?>">
                <div class="confirm-intro">
                    <p>入力内容にお間違いがないか確認の上、<br>「送信する」ボタンを押してください。</p>
                    <p>再度ご入力される場合は<br>「戻る」ボタンを押してください。</p>
                </div>
                <div class="confirm-inner">
                    <p><?php echo $name; ?></p>
                    <p><?php echo $mail; ?></p>
                    <p><?php echo $content; ?></p>
                </div>
                <div class="btn-group">
                    <input type="button" value="戻る" id="back" onclick="history.back(-1)">
                    <button type="submit" name="submit">送信する</button>
                </div>
            </form>
        </section>
    </main>
    <footer>
        <small>&copy; 5LDK ALL RIGHTS RESERVED.</small>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/contact.js"></script>
    <script type="text/javascript">
        $(function () {
            // ハンバーガーメニュー開閉
            $('#nav-toggle').on('click', function () {
                $('body').toggleClass('open');
            });
            $('#gloval-nav a[href]').on('click', function () {
                $('body').toggleClass('open');
            });
            // メインビジュアル＆メニューの高さを画面サイズに合わせる
            var heightSize = $(window).height();
            $('.section-mv').height(heightSize);
            $('.section-mv .mv-inner').height(heightSize);
            $('#gloval-nav').height(heightSize);
        });
    </script>
    <script>
        window.onload = function() {
            $('h3').addClass('isActive');
        }
    </script>
</body>

</html>